from django.db import models

# Create your models here.

class Genero(models.Model):
    nombre = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre

class Persona(models.Model):
    #id -> numero autoincremental  que se genera automáticamente
    nombre = models.CharField(max_length=100, verbose_name="Ingrese Nombre")
    apellido = models.CharField(max_length=100, verbose_name="Ingrese Apellido")
    rut = models.IntegerField(verbose_name="Rut sin guión")
    genero = models.ForeignKey(Genero ,on_delete = models.CASCADE)
    def __str__(self):
        return self.nombre + " " + self.apellido

class Registro(models.Model):
    #id -> numero autoincremental  que se genera automáticamente
    nombre = models.CharField(max_length=100 , verbose_name= "Ingrese Nombre")
    apellido= models.CharField(max_length=100 , verbose_name= "Ingrese Apellido")
    rut = models.IntegerField(verbose_name="Rut sin guión")
    nusuario = models.CharField(max_length=100 , verbose_name= "Nombre de usuario")
    email = models.CharField(max_length=100 , verbose_name= "Email")
    contrasena = models.CharField(max_length=100 , default= "", verbose_name= "Contraseña")
    genero = models.ForeignKey(Genero ,on_delete = models.CASCADE)
    def __str__(self):
        return self.nusuario

class Producto(models.Model):
    descripcion = models.CharField(max_length=255, blank=True,verbose_name= "Descripción de producto")
    ML = models.IntegerField(verbose_name="Cantidad de ML")
    imagen = models.FileField(upload_to='productos')
    def __str__(self):
        return self.descripcion

