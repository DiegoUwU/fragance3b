# Generated by Django 3.1.2 on 2020-11-11 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_delete_prueba'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='ML',
            field=models.IntegerField(default=1, verbose_name='Rut sin guión'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='producto',
            name='descripcion',
            field=models.CharField(blank=True, max_length=255, verbose_name='Descripción de producto'),
        ),
    ]
