from django.contrib import admin
from .models import Genero , Persona , Registro
# Register your models here.

class PersonaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido', 'rut','genero']
    search_fields = ['apellido', 'rut']
    list_filter = ['genero']
    list_per_page=10

class RegistroAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'apellido', 'rut','genero','email','nusuario']
    search_fields = ['apellido', 'rut','nusuario']
    list_filter = ['genero']
    list_per_page=10




admin.site.register(Genero)
admin.site.register(Persona, PersonaAdmin)
admin.site.register(Registro, RegistroAdmin)


