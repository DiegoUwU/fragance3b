from django.urls import path
from .views import Inicio, Listado, Contacto, QuienesSomos, InicioSesion,ListaRegistros, nuevoRegistro,modificarRegistro, eliminarRegistro
from .views import  InicioLOG, ContactoLOG, QuienesSomosLOG
urlpatterns = [
    path('', Inicio, name='Inicio'),
    path('contacto/', Contacto, name='Contacto'),
    path('quienes-somos/', QuienesSomos, name='Quienes Somos'),
    path('inicio-sesion/', InicioSesion, name='Inicio sesion',),
    path('lista-registro/', ListaRegistros, name='Lista Registros'),
    path('accounts/login/nuevo-registro/', nuevoRegistro, name='Nuevo Registros'),
    path('accounts/login/modificar-registro/<id>', modificarRegistro, name='Modificar registros'),
    path('accounts/login/eliminar-registro/<id>', eliminarRegistro, name='Eliminar registros'),
    path('accounts/login/inicio', InicioLOG, name='Inicio loged'),
    path('accounts/login/contacto', ContactoLOG, name='Contacto loged'),
    path('accounts/login/quienes-somos/', QuienesSomosLOG, name='Quienes somos Loged'),
    path('accounts/login/listado/', Listado, name='Listado Loged'),
]
