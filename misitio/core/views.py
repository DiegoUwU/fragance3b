from django.shortcuts import render, redirect
from .models import Persona
from .models import Registro,Producto
from .forms import RegistroForm 



# Create your views here.
def Inicio(request):
    return render(request,'core/Inicio.html')
def ListaRegistros(request):
    registro = Registro.objects.all()
    data={
        'registros':registro
    }
    return render(request,'core/ListaRegistros.html',data)

def nuevoRegistro(request):
    data = {
        'form' : RegistroForm()
    }
    if request.method == 'POST':
        formulario = RegistroForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = 'Datos guardados correctamente'
        else:
            data['mensaje'] = 'Datos no pudieron ser guardados '
    return render(request,'core/nuevoRegistro.html',data)

def Listado(request):
    return render(request,'core/logged/Listado.html')

def Contacto(request):
    return render(request,'core/Contacto.html')
def QuienesSomos(request):
    return render(request,'core/QuienesSomos.html')
def InicioSesion(request):
    return render(request,'core/InicioSesion.html')
def modificarRegistro(request, id):
    registro = Registro.objects.get(id = id)
    data = {
        'form' : RegistroForm(instance=registro)
    }
    if request.method == 'POST':
        formulario = RegistroForm(data= request.POST,instance=registro)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = 'Los datos modificados correctamente.'
            data['form'] = formulario
        else:
            data['mensaje'] = 'Los datos no pudieron ser modificados. '
   
    return render(request,'core/modificarRegistro.html',data)
    
def eliminarRegistro(request, id):
    registro = Registro.objects.get(id = id)
    registro.delete()
    return redirect (to = 'Lista Registros')
    




def InicioLOG(request):
    return render(request,'core/logged/InicioLOG.html')
    
def ContactoLOG(request):
    return render(request,'core/logged/ContactoLOG.html')
def QuienesSomosLOG(request):
    return render(request,'core/logged/QuienesSomosLOG.html')   

